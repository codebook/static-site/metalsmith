var Metalsmith  = require('metalsmith');
var markdown    = require('metalsmith-markdown');
var layouts     = require('metalsmith-layouts');
var permalinks  = require('metalsmith-permalinks');

// export class MetalsmithSiteModule {
//   constructor() {
//     console.log('MetalsmithSiteModule loaded....');
//   }
// }


Metalsmith(__dirname)
  .metadata({
    title: "My Test Static Site",
    description: "It's about nothing.",
    generator: "Metalsmith",
    url: "http://gitlab.com/codebook/metalsmith"
  })
  .source('../input')
  .destination('../output')
  .clean(false)
  .use(markdown())
  .use(permalinks())
  .use(layouts({
    engine: 'handlebars',
    directory: '../layouts'
  }))
  .build(function(err, files) {
    if (err) { throw err; }
  });
