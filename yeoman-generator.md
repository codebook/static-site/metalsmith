# Yeoman Generator metalsmith-basic

_Yeoman Generator, generator-metalsmith-basic, for a Metalsmith static website in Typescript._



This generator creates an absolutely minimal Metalsmith website skeleton (in typescript).



## Installation

First, install [Yeoman](http://yeoman.io) and [generator-metalsmith-basic](https://www.npmjs.com/package/generator-metalsmith-basic) using [npm](https://www.npmjs.com/).
(You'll obviously need [node.js](https://nodejs.org/) installed on your system. Bower is optional.).

```bash
npm install -g yo typescript
npm install -g generator-metalsmith-basic
```

Then generate your new project:

```bash
mkdir my-metal-project
cd my-metal-project
yo metalsmith-basic
```


## Manual Build

```bash
make build
```





